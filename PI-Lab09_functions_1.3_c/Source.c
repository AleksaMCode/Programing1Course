#include <stdio.h>
#include <math.h>
#include <string.h>
#define SIZE 20
typedef struct polynomial { int deg; int array[SIZE]; }POLYNOMIAL;

POLYNOMIAL read();
POLYNOMIAL add(POLYNOMIAL*, POLYNOMIAL*); /* Addition */
POLYNOMIAL mul(POLYNOMIAL*, POLYNOMIAL*); /* Multiplication */
void print(POLYNOMIAL);

int main()
{
	POLYNOMIAL p1, p2;
	p1 = read();
	p2 = read();
	printf("\n\nPOL1: "), print(p1);
	printf("POL2: "), print(p2);
	printf("SUM:  "); print(add(&p1, &p2));
	printf("MUL:  "); print(mul(&p1, &p2));

	return 0;
}

POLYNOMIAL read()
{
	static short k;
	++k;
	POLYNOMIAL temp = { 0,0 };
	do { printf("\nEnter the highest degree of the %d. polynomial: ", k), scanf("%d", &temp.deg); } while (temp.deg < 0 || temp.deg >= SIZE);

	/* Read the coefficients into an array */
	for (short i = temp.deg; i >= 0; --i)
		printf("Enter polynomial coefficient of x^%d : ", i),
		scanf("%d", &temp.array[i]);

	return temp;
}

POLYNOMIAL add(POLYNOMIAL* pol, POLYNOMIAL* pol2)
{
	POLYNOMIAL temp = { 0,0 };
	short min = 0;
	
	if (pol->deg > pol2->deg) min = pol2->deg, temp.deg = pol->deg, memcpy(temp.array, pol->array, sizeof(int)), temp.array[temp.deg] = pol->array[pol->deg]; 
	else min = pol->deg, temp.deg = pol2->deg, memcpy(temp.array, pol2->array, sizeof(int)), temp.array[temp.deg] = pol2->array[pol2->deg];
	
	for (short i = 0; i <= min; ++i)
		temp.array[i] = pol->array[i] + pol2->array[i];
	
	return temp;
}

POLYNOMIAL mul(POLYNOMIAL* pol, POLYNOMIAL* pol2)
{
	POLYNOMIAL temp = { pol->deg + pol2->deg, 0 };
	for (int i = 0; i <= pol->deg; i++)
		for (int j = 0; j <= pol2->deg; j++) {
			temp.array[i + j] += pol->array[i] * pol2->array[j];
		}
	return temp;
}

void print(POLYNOMIAL pol)
{
	for (int i = 0; i <= pol.deg; ++i)
	{
		/* Printing proper polynomial function */
		if (pol.array[i] > 0) printf(" + ");
		else if (pol.array[i] < 0) printf(" - ");
		else { printf("         "); continue; }
		printf("%dx^%d  ", abs(pol.array[i]), i);
	}
	printf("\n");
}

/* Code by: Aleksa M. */