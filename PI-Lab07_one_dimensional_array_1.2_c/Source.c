#include <stdio.h>
#include <math.h>
#include <ctype.h>
#define SIZE 100
#define POW(a) (a)*(a)

int main()
{
	int nElements, iPoint1[SIZE] = { 0 }, iPoint2[SIZE] = { 0 };
	double euclideanDistance = 0.0;
	
	do printf("Enter hyperspace dimension: "), scanf("%d", &nElements); while (nElements < 0 || nElements > SIZE);

	printf("\nFirst point:\n");
	for (int i = 0; i < nElements; ++i)
		printf("%d. element: ", i + 1), scanf("%d", &iPoint1[i]);

	printf("\nSecond point:\n");
	for (int i = 0; i < nElements; ++i)
		printf("%d. element: ", i + 1), scanf("%d", iPoint2 + i), euclideanDistance += POW(iPoint1[i] - iPoint2[i]);

	char c;
	int count = 2;
	do
	{
		printf("\nEnter a new point? (y\\n) "), scanf(" %c", &c);
		if (tolower(c) == 121)
		{
			for (int i = 0; i < nElements; ++i)
				iPoint1[i] = *(iPoint2 + i);

			printf("\n%d. point:\n", ++count);
			for (int i = 0; i < nElements; ++i)
				printf("%d. element: ", i + 1), scanf("%d", &iPoint2[i]), euclideanDistance += POW(iPoint1[i] - iPoint2[i]);
		}
		else if (tolower(c) != 0x6e) return printf("ERROR!\n");
	} while (tolower(c) == 'y');

	printf("\nEuclidean Distance = %.3lf\n", euclideanDistance = sqrt(euclideanDistance));

	return 0;
}

/* Code by: Aleksa M. */
