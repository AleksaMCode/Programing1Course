#include <stdio.h>
#define SIZE 10

void read(int[][10], int*, int*);
void print(int[][10], int, int);
void multiplyMatrix(int[][10], int[][10], int[][10], int, int, int, int); /* solved using recursion */

int main()
{
	int n, m, k, h;
	int mx1[SIZE][SIZE] = { 0 }, mx2[SIZE][SIZE] = { 0 }, mul[SIZE][SIZE] = { 0 };
	read(mx1, &n, &m);
	read(mx2, &k, &h);
	printf("\n   1st MATRIX:\n"), print(mx1, n, m);
	printf("\n   2nd MATRIX:\n"), print(mx2, k, h);
	if (m == k) multiplyMatrix(mx1, mx2, mul, n, m, k, h), printf("\n MULTIPLY MATRIX:\n"), print(mul, n, k);
	else return printf("\nRequirement for matrix multiplication is not met\n"), 1;

	return 0;
}

void read(int matrix[][10], int* n, int* m)
{
	static short num;
	++num;
	do printf("\n%d. MATRIX:\nInsert matrix dimension (nxm): ", num), scanf("%dx%d", n, m); while (*n <= 0 || *n > SIZE || *m <= 0 || *m > SIZE);
	for (int i = 0; i < *n * *n; ++i)
		printf("matrix[%d][%d] = ", i / *n, i % *n), scanf("%d", &matrix[i / *n][i % *n]);
}

void print(int matrix[][10], int n, int m)
{
	for (short i = 0; i < (short)n; ++i, printf("\n"))
		for (short j = 0; j < (short)m; ++j)
			printf("%4.0d ", *(*(matrix + i) + j));
}

void multiplyMatrix(int matrix1[][10], int matrix2[][10], int mul[][10], int m, int n, int o, int p)
{
	static int sum, i = 0, j = 0, k = 0;

	if (i < m) /* row of first matrix */
	{
		if (j < p) /* column of second matrix */
		{
			if (k < n)
			{
				sum = sum + matrix1[i][k] * matrix2[k][j];
				k++;
				multiplyMatrix(matrix1, matrix2, mul, m, n, o, p);
			}
			mul[i][j] = sum;
			sum = 0;
			k = 0;
			j++;
			multiplyMatrix(matrix1, matrix2, mul, m, n, o, p);
		}
		j = 0;
		i++;
		multiplyMatrix(matrix1, matrix2, mul, m, n, o, p);
	}
}

/* Code by: Aleksa M. */