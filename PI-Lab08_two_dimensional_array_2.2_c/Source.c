#include <stdio.h>
#include <math.h>
#include <string.h>
#define SIZE 20
typedef unsigned char uc;
#define printMx(n) printf ("%3.0d", matrix ## n)
#define printAr(x) printf("%d ", arrayHelp ## x)
#define SHELL_SORT(arr, n) \
for (uc h = n / 2; h > 0; h /= 2) \
for (uc l = h, e; l < n; ++l) { \
	int x = arr[l]; \
	for (e = l; e >= h && x < arr[e - h]; e -= h) \
		arr[e] = arr[e - h]; \
	arr[e] = x; \
} \

int main()
{
	int matrix[SIZE][SIZE] = { 0 }, numOfElements;
	do printf("Insert matrix dimension: "), scanf("%d", &numOfElements); while (numOfElements < 1 || numOfElements > SIZE);

	for (uc i = 0; i < numOfElements; ++i)
		for (uc j = 0; j < numOfElements; ++j)
			printf("matrix[%hu][%hu]: ", i + 1, j + 1), scanf("%d", &matrix[i][j]);

	printf("\n  MATRIX:\n");
	for (uc i = 0; i < numOfElements; printf("\n"), ++i)
		for (uc j = 0; j < numOfElements; ++j)
			printMx([i][j]);

	int arrayHelp[SIZE] = { 0 };
	int n = 0;
	for (uc i = 0; i < numOfElements; i++)
		for (uc j = i + 1; j < numOfElements; j++)
			arrayHelp[n++] = matrix[i][j];

	SHELL_SORT(arrayHelp, n);
	printf("\nUpper triangular matrix (sorted ascending): ");
	for (int i = 0; i < n; ++i) printAr([i]);
	memset(arrayHelp, n = 0, SIZE);

	for (int i = 1; i < numOfElements; i++)
		for (int j = 0; j < i; j++)
			arrayHelp[n++] = matrix[i][j];

	SHELL_SORT(arrayHelp, n);
	printf("\nLower triangular matrix (sorted ascending): ");
	for (int i = 0; i < n; ++i) printAr([i]);

	return 0;
}

/* Code by: Aleksa M. */