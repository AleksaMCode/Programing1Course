#include <stdio.h>
#include <math.h>
#define SIZE 9
#define NL printf("\n")
typedef unsigned char uc;
typedef struct { int R, G, B; }RGB;

void matrixRead(RGB*, uc, uc);

int main(void)
{
	RGB matrix[SIZE][SIZE] = { 0 };
	int numOfElements;
	do printf("Insert matrix dimension: "), scanf("%d", &numOfElements); while (numOfElements < 1 || numOfElements > SIZE);

	printf("Enter the RGB components of image pixels:\n");
	for (uc i = 0; i < numOfElements; ++i)
		for (uc j = 0; j < numOfElements; ++j)
			matrixRead(&matrix[i][j], i, j);
	
	printf("\nPicture:\n");
	for (uc i = 0; i < numOfElements; ++i, NL)
		for (uc j = 0; j < numOfElements; ++j)
			printf("#%02x%02x%02x ", matrix[i][j].R, matrix[i][j].G, matrix[i][j].B);

	return 0;
}

void matrixRead(RGB* rgb, uc i, uc j)
{
	do printf("pixel(%hu,%hu) - R = ", i, j), scanf("%d", &(*rgb).R); while (rgb->R < 0 || rgb->R > 255);
	do printf("pixel(%hu,%hu) - G = ", i, j), scanf("%d", &(*rgb).G); while (rgb->G < 0 || rgb->G > 255);
	do printf("pixel(%hu,%hu) - B = ", i, j), scanf("%d", &(*rgb).B); while (rgb->B < 0 || rgb->B > 255);
}

/* Code by: Aleksa M. */