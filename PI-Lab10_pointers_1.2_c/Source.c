#include <stdio.h>
#include <stdlib.h>
#define printarr(fmt, dat, len) \
for (int i = 0; i < len; ++i) \
	printf(fmt, i + 1, dat[i].name, dat[i].scored_goals, dat[i].received_gols, dat[i].scored_goals - dat[i].received_gols, dat[i].points); \

typedef struct { char name[20]; int received_gols, scored_goals, points; }TEAM;

void read(TEAM*);
TEAM* form(int*);
void print(TEAM*, int);

/* --------BUCKET-SORT-------- */
struct bucket
{
	int count;
	TEAM* values;
};

int compareTeamByCriteria(const void* first, const void* second);
void bucketSort(TEAM* array, int n);
/* -------------------------- */

int main()
{
	int n;
	do printf("How many teams do u wish to insert? "), scanf("%d", &n); while (n <= 0);
	TEAM* array = form(&n);
	for (int i = 0; i < n; ++i)
		printf("%d. TEAM:\n", i + 1), read(&array[i]);
	bucketSort(array, n);
	print(array, n);

	/* Deallocate dynamic array */
	free(array);
	return 0;
}

void read(TEAM* team)
{
	printf("NAME: "); scanf("%s", team->name);
	printf("SCORED GOALS: "); scanf("%d", &team->scored_goals);
	printf("RECEIVED GOALS: "); scanf("%d", &team->received_gols);
	printf("POINTS: "); scanf("%d", &team->points);
}

TEAM* form(int* n)
{
	return (TEAM*)malloc(*n * sizeof(TEAM));
}

void print(TEAM* array, int n)
{
	printf("==== ================ ====== ====== ====== ======\n");
	printf("NUM. NAME             GSC    GRE    GD     POINTS\n");
	printf("==== ================ ====== ====== ====== ======\n");
	printarr("%3d. %-16s %6d %6d %6d %6d\n", array, n);
	printf("==== ================ ====== ====== ====== ======\n");
}

/* --------BUCKET-SORT-------- */
int compareTeamByCriteria(const void* first, const void* second)
{
	const TEAM* a = (TEAM*)first, *b = (TEAM*)second;
	if (a->points == b->points)
		if ((a->scored_goals - a->received_gols) > (b->scored_goals - b->received_gols))
			return -1;
		else if ((a->scored_goals - a->received_gols) < (b->scored_goals - b->received_gols))
			return 1;
		else return 0;
	else if (a->points < b->points)
		return 1;
	else
		return -1;
}

void bucketSort(TEAM* array, int n)
{
	struct bucket buckets[3];
	int i, j, k;
	for (i = 0; i < 3; i++)
	{
		buckets[i].count = 0;
		buckets[i].values = (TEAM*)malloc(sizeof(TEAM) * n);
	}
	/* Divide the unsorted elements among 3 buckets */
	/* > 25   : first */
	/* 0 - 25 : second */
	/* < 0    : third */
	for (i = 0; i < n; i++)
		if (array[i].points > 25)
			buckets[0].values[buckets[0].count++] = array[i];
		else if (array[i].points < 0)
			buckets[2].values[buckets[2].count++] = array[i];
		else
			buckets[1].values[buckets[1].count++] = array[i];

	for (k = 0, i = 0; i < 3; i++)
	{
		/* Use Quicksort to sort each bucket individually */
		qsort(buckets[i].values, buckets[i].count, sizeof(TEAM), compareTeamByCriteria);
		for (j = 0; j < buckets[i].count; j++)
			array[k + j] = buckets[i].values[j];
		k += buckets[i].count;
		free(buckets[i].values);
	}
}
/* -------------------------- */

/* Code by: Aleksa M. */
