#include <stdio.h>
#define SIZE 15
#define SWAP(a,b) {		  \
					a^=b; \
					b^=a; \
					a^=b; \
				  }

int main()
{
	int array[SIZE];
	for (int i = 0; i < SIZE; ++i) array[i] = i + 1;

	printf("ARRAY:\n%d", array[0]);
	for (int i = 1; i < SIZE; ++i) printf(", %d", array[i]);

	SWAP(array[0], array[SIZE - 1]);
	printf("\n\nREVERSE ARRAY:\n%d", array[0]);
	for (int i = 1; i < SIZE / 2; ++i)
	{
		SWAP(array[SIZE - i - 1], array[i]);
		printf(", %d", array[i]);
		if (i == SIZE / 2 - 1)
		{
			while (++i < SIZE)
				printf(", %d", array[i]);
		}
	}

	return 0;
}

/* Code by: Aleksa M. */