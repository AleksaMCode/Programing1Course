#include <stdio.h>
#include <stdlib.h>
#include <string.h>

void read(char*); /* delimiter = '\n' */
void read2(char*);
int strlen_(char*);
void printFirstSentence(char*); /* !!!!!!delimiter = '.' */

int main()
{
	char string[100];
	read(string);
	if (!strstr(string, ".")) return 1;
	printFirstSentence(string);

	printf("\n\n");
	read2(string);
	if (!strstr(string, ".")) return 1;
	printFirstSentence(string);

	return 0;
}

void read(char* str)
{
	fgets(str, 100, stdin);
}

void read2(char* str)
{
	scanf("%[^\n]s", str);
}

int strlen_(char* str)
{
	unsigned char i = 0;
	while (*(str + i++));
	return (int)--i;
}

void printFirstSentence(char* str)
{
	char* first_sentence = /* (char*) */calloc(strlen_(str) + 1, sizeof(char));
	unsigned char i = 0;
	while (*(str + i) != '.')
		first_sentence[i] = str[i++];

	first_sentence = /* (char*) */ realloc(first_sentence, (i + 1) * sizeof(char));
	printf("%s (length = %d)\n", first_sentence, strlen_(first_sentence));
	free(first_sentence);
}

/* Code by: Aleksa M. */