#include <stdio.h>
typedef unsigned short int usi;
typedef short int si;
typedef unsigned int ui;

void main()
{
	usi lowerLimit, upperLimit; /* a = lowerLimit, b = upperLimit */
	double sn = 0.0, numerator = 0.0, denominator = 0.0;

	do printf("Enter limits: "), scanf("%hu,%hu", &lowerLimit, &upperLimit); while (lowerLimit >= upperLimit || lowerLimit <= 0 || upperLimit <= 0);

	for (usi i = lowerLimit; i <= upperLimit; printf("S%hu = %.4lf\n", i++, sn))
	{
		numerator = i, denominator = 0.0; /* brojilac = numerator, imenilac = denominator */
		{
			si iCopy = (si)i;
			while (iCopy) denominator += (double)iCopy / (iCopy-- + 1);
		}

		for (usi j = 1; j < i; ++j) numerator *= i;
		numerator /= denominator;
		sn += numerator;
	}
}

/* Code by: Aleksa M. */