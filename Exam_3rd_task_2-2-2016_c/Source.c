/* GROUP A */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#define BOOK_SIZE sizeof(BOOK)

typedef struct book { char ISBN[14], title[15]; int state; }BOOK;
typedef struct bookstore { char name[20]; int availableBooks; BOOK* array; }BOOKSTORE;

BOOK* read_books(int*);
void read_bookstore(BOOKSTORE*);
void sorting(BOOK*, int); /* cocktail shaker sort : best/average O(n), worst O(n^2) */
void buy(BOOKSTORE*, char*);
void print(BOOKSTORE*);

int k; /* number of different books */

int main()
{
	struct bookstore bookStore = { "",0,(void*)0 };
	read_bookstore(&bookStore);
	char title[15];
	system("CLS"), printf("Name of the book u want to buy? "), scanf("%s", title);
	buy(&bookStore, title);
	sorting(bookStore.array, k);
	printf("\n");
	print(&bookStore);

	free(bookStore.array);
	return 0;
}

BOOK* read_books(int* size)
{
	printf("How many books do u want to enter? "), scanf("%d", &*size);
	struct book* arr = malloc(BOOK_SIZE ** size);
	int j, duplicate = 0;
	char isbn[14];
	enum boolean { false, true } doesItContainDuplicates = false;

	for (int i = 0; i < *size; i++)
	{
		printf("%d. BOOK:\n", ++i);
		do printf("ISBN: "), scanf("%s", isbn); while (strlen(isbn) != 13 || strpbrk(isbn, "qwertyuiopasdfghjklzxcvbnm`[];'\\,./!@#$%^&*()_+"));
		enum boolean check = false;

		for (j = 0; j < i - 1 - duplicate; j++) /* sequential search */
		{
			if (!strcmp(arr[j].ISBN, isbn))
			{
				check = (enum boolean)1;

				if (!doesItContainDuplicates)
					doesItContainDuplicates = true;

				break;
			}
		}

		if (check)
		{
			arr[--i, j].state++;
			duplicate++;
			continue;
		}
		else
		{
			int index = --i;
			if (doesItContainDuplicates)
				index = i - duplicate;

			arr[index].state = 1;
			strcpy(arr[index].ISBN, isbn);
			printf("TITLE: "), scanf("%s", (arr + index)->title);
		}
	}

	arr = realloc(arr, (k = -(duplicate -* size)) * BOOK_SIZE);
	return arr;
}

void read_bookstore(BOOKSTORE* bStore)
{
	int n;
	printf("NAME OF A BOOKSTORE: "), scanf("%s", bStore->name);
	bStore->array = read_books(&n);
	bStore->availableBooks = n;
}

void sorting(BOOK* bArray, int n)
{
	int swapped;
	do
	{
		register int i;
		swapped = 0;
		for (i = n - 1; i > 0; i--)
			if (strcmp((*(bArray + i - 1)).ISBN, (*(bArray + i)).ISBN) < 0)
			{
				BOOK temp = bArray[i - 1];
				bArray[i - 1] = bArray[i];
				bArray[i] = temp;
				swapped = 1;
			}

		for (i = 1; i < n; i++)
			if (strcmp((*(bArray + i - 1)).ISBN, (*(bArray + i)).ISBN) < 0)
			{
				BOOK temp = bArray[i - 1];
				bArray[i - 1] = bArray[i];
				bArray[i] = temp;
				swapped = 1;
			}
	} while (swapped);
}

void buy(BOOKSTORE* store, char* title)
{
	int found = 0;

	for (int i = 0; i < k; i++)
		if (!strcmp(store->array[i].title, title))
		{
			if (store->array[i].state)
			{
				store->array[i].state--;
				store->availableBooks--;
				found = 1;
			}
			break;
		}

	if (found)
		printf("You have successfully bought a book.\n");
	else
		printf("There is either no book by the name '%s' or we do not have a copy of it at the moment.", title);
}

void print(BOOKSTORE* store)
{
	printf("BOOKSTORE: %s\nNUMBER OF BOOKS IN A STORE: %d\nBOOKS:\n", store->name, store->availableBooks);
	printf("NUM. ISBN          TITLE           STATE\n");
	printf("---- ------------- --------------- -----\n");
	for (int i = 0; i < k; i++)
		printf("%3d. %-13s %-15s %5d\n", i + 1,
			store->array[i].ISBN, store->array[i].title, store->array[i].state);
	printf("---- ------------- --------------- -----\n");
}

/* Code by: Aleksa M. */