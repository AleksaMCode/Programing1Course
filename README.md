Welcome
-------------------

Collection of codes I wrote in C for course Programing 1.


EXAMS
-------------------------------
* [1. midterm exam - 29.11.2016. (1. task)](https://gitlab.com/AleksaMCode/Programing1Course/blob/master/1st_midterm_exam_1st_task_29-11-2016_c/Source.c)
* [Exam - 1.7.2016. (4th task)](https://gitlab.com/AleksaMCode/Programing1Course/blob/master/Exam_4th_task_1-7-2016_c/Source.c)
* [Exam - 2.2.2016. (3rd task) - Group A aka 2. midterm exam (1st task)](https://gitlab.com/AleksaMCode/Programing1Course/blob/master/Exam_3rd_task_2-2-2016_c/Source.c) 
* [Exam - 2.2.2016. (4th task) - Group A aka 2. midterm exam (2nd task)](https://gitlab.com/AleksaMCode/Programing1Course/blob/master/Exam_4th_task_2-2-2016_c/Source.c)
* [Exam - 2.2.2016. (4th task) - Group B aka 2. midterm exam (2nd task)](https://gitlab.com/AleksaMCode/Programing1Course/blob/master/Exam_4th_task_2-2-2016_gB_c/Source.c)
* [Exam - 7.7.2015. (3rd task)](https://gitlab.com/AleksaMCode/Programing1Course/blob/master/Exam_4th_task_7-7-2015_c/Source.c)

LABORATORY
-------------------------------
#### LAB07 - One dimensional array
* 1.1 [Reverse array of ints](https://gitlab.com/AleksaMCode/Programing1Course/blob/master/PI-Lab07_one_dimensional_array_1.1_c/Source.c)
* 1.2 [Euclidean distance hyperspace calculator for 2 points](https://gitlab.com/AleksaMCode/Programing1Course/blob/master/PI-Lab07_one_dimensional_array_2.1_c/Source.c)
* 1.3 [Array of ints, arithmetic mean, sort (bubble + selection)](https://gitlab.com/AleksaMCode/Programing1Course/blob/master/PI-Lab07_one_dimensional_array_1.3_c/Source.c)
* 2.1 [Euclidean distance hyperspace, calculator for n points](https://gitlab.com/AleksaMCode/Programing1Course/blob/master/PI-Lab07_one_dimensional_array_2.1_c/Source.c)
* 2.2 [Array of ints, arithmetic mean, sort (bubble + selection) - MOD of 1.3](https://gitlab.com/AleksaMCode/Programing1Course/blob/master/PI-Lab07_one_dimensional_array_2.2_c/Source.c)

#### LAB08 - Two dimensional array
* 1.2 [Sum of main diagonal and sum of antidiagonal](https://gitlab.com/AleksaMCode/Programing1Course/blob/master/PI-Lab08_two_dimensional_array_1.2_c/Source.c)
* 1.3 [Sort matrix in descending order (shell sort)](https://gitlab.com/AleksaMCode/Programing1Course/blob/master/PI-Lab08_two_dimensional_array_1.3_c/Source.c)
* 1.4 [Find prime numbers in a matrix](https://gitlab.com/AleksaMCode/Programing1Course/blob/master/PI-Lab08_two_dimensional_array_1.4_c/Source.c)
* 1.5 [Manipulation of the image in a raster graphics](https://gitlab.com/AleksaMCode/Programing1Course/blob/master/PI-Lab08_two_dimensional_array_1.5_c/Source.c)
* 2.1 [Sums in a matrix](https://gitlab.com/AleksaMCode/Programing1Course/blob/master/PI-Lab08_two_dimensional_array_2.1_c/Source.c)
* 2.2 [Upper and lower triangular matrix in ascending order sorted (shell sort)](https://gitlab.com/AleksaMCode/Programing1Course/blob/master/PI-Lab08_two_dimensional_array_2.2_c/Source.c)
* 2.3 [Two-dimensional array, print elements that are prime numbers and are bigger than arithmetic mean of antidiagonal](https://gitlab.com/AleksaMCode/Programing1Course/blob/master/PI-Lab08_two_dimensional_array_2.3_c/Source.c)

#### LAB09 - Functions
* 1.1 [One-dimensional array, print prime numbers](https://gitlab.com/AleksaMCode/Programing1Course/blob/master/PI-Lab09_functions_1.1_c/Source.c)
* 1.2 [Matrix multiplication using recursion](https://gitlab.com/AleksaMCode/Programing1Course/blob/master/PI-Lab09_functions_1.2_c/Source.c)
* 1.3 [Polynomial – representation using struct, Addition, Multiplication](https://gitlab.com/AleksaMCode/Programing1Course/blob/master/PI-Lab09_functions_1.3_c/Source.c)
* 2.1 [One-dimensional array, print sorted arrays, all numbers, only prime, and only perfect, using 3 different arrays. Sort arrays using C library function - qsort()](https://gitlab.com/AleksaMCode/Programing1Course/blob/master/PI-Lab09_functions_2.1_c/Source.c)

#### LAB10 - Pointers
* 1.1 [Using struct POINT, if possible create a TRIANGLE](https://gitlab.com/AleksaMCode/Programing1Course/blob/master/PI-Lab10_pointers_1.1_c/Source.c)
* 1.2 [Create dynamic array of struct TEAM, sort it descending using bucket sort and print out the results](https://gitlab.com/AleksaMCode/Programing1Course/blob/master/PI-Lab10_pointers_1.2_c/Source.c)
* 2.1 [Using struct POINT, if possible create a TRIANGLE. MODIFICATION: Calculate area and circumference, and check if the fourth point is in a triangle (solve using Barycentric coordinate system)](https://gitlab.com/AleksaMCode/Programing1Course/blob/master/PI-Lab10_pointers_2.1_c/Source.c)

#### LAB11 - Strings
* 1.1 [Load one line from standard input (including spaces). The line ends with character new line ( '\ n') and it isn't longer than 100 characters. Print first sentences and its length (implying that the string contains at least one sentence)](https://gitlab.com/AleksaMCode/Programing1Course/blob/master/PI-Lab11_strings_1.1_c/Source.c)
* 1.2 [Insert a word and then print its vowels](https://gitlab.com/AleksaMCode/Programing1Course/blob/master/PI-Lab11_strings_1.2_c/Source.c)
* 1.3 [Write a function that accepts the word as a string and the length of the word, and change the first letter of a word to uppercase letter and the remaining letters to lowercase letters](https://gitlab.com/AleksaMCode/Programing1Course/blob/master/PI-Lab11_strings_1.3_c/Source.c)
* 1.4 [Find and print indexes of all words (first-letter in a word) in the first sentence loaded in a string](https://gitlab.com/AleksaMCode/Programing1Course/blob/master/PI-Lab11_strings_1.4_c/Source.c)
* 1.5 [Insert number n, then load n words from standard input. The word is no longer than 100 characters. Dynamically allocated array of loaded words like array of pointers to character strings (dynamic array needs to have a type char **). Print out an array of words in one line with spaces between them](https://gitlab.com/AleksaMCode/Programing1Course/blob/master/PI-Lab11_strings_1.5_c/Source.c)
* 2.1 [Load lines from standard input until it appears a line that begins with a '#'. The number of lines will not exceed 50. Then perform the concatenation of the given lines in one array and then print the string to the standard output so that each sentence begins with a capital letter, and all other words are written in small letters](https://gitlab.com/AleksaMCode/Programing1Course/blob/master/PI-Lab11_strings_2.1_c/Source.c)
* 2.2 [Count the total number of words in the whole text loaded](https://gitlab.com/AleksaMCode/Programing1Course/blob/master/PI-Lab11_strings_2.2_c/Source.c)
* 2.3 [Find a word that most often occurs in a loaded text. It is understood that the text will not contain more than 1000 words](https://gitlab.com/AleksaMCode/Programing1Course/blob/master/PI-Lab11_strings_2.3_c/Source.c)

Helping
----------------

#### I found a bug!

If you found a bug send me an email.
Include steps to consistently reproduce the problem, actual vs. expected
results, screenshots.


Contact info
----------------------------------------------

* **E-mail:** 
 [Aleksa Majkić (aleksamcode@gmail.com)](mailto:aleksamcode@gmail.com)