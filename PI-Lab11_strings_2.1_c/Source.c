#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#define RESIZE_CONST 100

void read(char*); /* delimiter = '#', max 50 lines */

int main()
{
	char* string = calloc(101, sizeof(char));
	read(string);

	if (strstr(string, "."))
	{
		for (char* p = string; *p; ++p) *p = tolower(*p);
		while (!isalpha(*string)) string++;
		*string = toupper(*string);
	}
	char* pstr = string;
	while (string = strstr(string, "."))
	{
		if (*(string + 2) != '\0')
		{
			while (!isalpha(*string)) string++;
			*string = toupper(*string);
		}
		else string++;
	}
	printf("%s", pstr);
	free(string);
	return 0;
}

void read(char* str)
{
	/* scanf("%50[^#]s", str); */
	unsigned int fill = 0, size = 101, nlines = 0;
	char sline[100];
	fgets(sline, 100, stdin), sline[strlen(sline) - 1] = 0;
	fill = strlen(sline) + 1;
	if (*sline != '#')
	{
		strcpy(str, sline), strcat(str, " "), ++nlines;
		do
		{
			fgets(sline, 100, stdin), sline[strlen(sline) - 1] = 0;
			++nlines;
			if (*sline != '#')
			{
				if (fill == size || fill + 1 + strlen(sline) >= size)
					str = realloc(str, (RESIZE_CONST + 1) * sizeof(char)), size += RESIZE_CONST + 1;
				fill += strlen(sline) + 1;
				strcat(str, sline);
				strcat(str, " ");
			}
		} while (*sline != '#' || nlines == 50);
		str[fill + 1] = 0;
	}
}

/* Code by: Aleksa M. */