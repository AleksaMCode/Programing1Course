#include <stdio.h>
#include <string.h>
#include <ctype.h>
#include <stdbool.h>

bool isVowel(char);
void printVowels(char*);
void printVowels2(char*);

int main()
{
	char word[50];
	printf("WORD: ");
	scanf("%s", word);
	printVowels(word);
	printf("\n");
	printVowels2(word);

	return 0;
}

bool isVowel(char c)
{
	switch (c)
	{
	case 'a': case 'e': case'i': case 'o': case 'u': return true;
	default: return false;
	}
}

void printVowels(char* str)
{
	unsigned char i = 0;
	while (*(str + i))
		if (isVowel(tolower(*(str + i++))))
			printf("%c ", *(str + i - 1));
}

void printVowels2(char* str)
{
	for (unsigned char i = 0; i < strlen(str); ++i)
		if (isVowel(tolower(str[i])))
			printf("%c ", str[i]);
}

/* Code by: Aleksa M. */