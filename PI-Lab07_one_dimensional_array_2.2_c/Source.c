#include <stdio.h>
#include <math.h>
#define SIZE 50
#define SWAP(a,b) {a^=b; b^=a; a^=b;}
/* TESTED WITH: 8 8 7 9 9 6 6 5 5 9 8 9 1 (total = 13) & 1 6 4 7 8 3 5 10 4 6 8 2 (total = 12) */

int main()
{
	int array[SIZE], n;
	double arithmetic_mean = 0.0;

	do printf("Size of an array? "), scanf("%d", &n); while (n < 0 || n > SIZE);

	for (int i = 0; i < n; ++i)
		printf("%d. element: ", i + 1), scanf("%d", array + i), arithmetic_mean += array[i];
	arithmetic_mean /= n;

	printf("INITIAL ARRAY: ");
	for (int i = 0; i < n; ++i)
		printf("%d ", *(array + i));

	int closest = *array;
	{
		double dist = fabs(*array - arithmetic_mean);
		for (int i = 1; i < n; ++i)
			if (fabs(*(array + i) - arithmetic_mean) < dist) dist = fabs(*(array + i) - arithmetic_mean), closest = *(array + i);
	}

	for (int i = 0; i < n - 1; i++) /* BUBBLE SORT */
		for (int j = 0; j < n - i - 1; j++)
			if (*(array + j) > *(array + j + 1)) SWAP(*(array + j), *(array + j + 1));

	for (int i = 0; i < n - 1; i++) /* SELECT(ION) SORT */
		if (*(array + i) > arithmetic_mean && *(array + i) != closest)
		{
			int max = i;
			for (int j = i + 1; j < n; j++)
				if (*(array + j) > *(array + max)) max = j;
			if (max != i) SWAP(*(array + i), *(array + max));
		}

	printf("\nOBTAINED ARRAY: ");
	for (int i = 0; i < n; ++i)
		printf("%d ", *(array + i));
	printf("(arithmetic mean %.2lf, closest %d)", arithmetic_mean, closest);

	return 0;
}

/* Code by: Aleksa M. */