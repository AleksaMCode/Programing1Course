#include <stdio.h>
#define POW(x) (x)*(x)
#define SWAP(a,b) {a^=b; b^=a; a^=b;}
#define SIZE 20
typedef unsigned char uc;
typedef unsigned short us;

int main()
{
	int matrix[SIZE][SIZE], numOfElements;
	do printf("Insert matrix dimension: "), scanf("%d", &numOfElements); while (numOfElements < 1 || numOfElements > SIZE);

	for (uc i = 0; i < numOfElements; ++i)
		for (uc j = 0; j < numOfElements; ++j)
			printf("matrix[%hu][%hu]: ", i + 1, j + 1), scanf("%d", &matrix[i][j]);

	/* SHELL SORT - descending order */
	uc flag = 1;
	us d = POW(numOfElements);
	while (flag || (d > 1)) /* boolean flag (true when not equal to 0) */
	{
		flag = 0, /* reset flag to 0 to check for future swaps */ d = (d + 1) / 2;
		for (us i = 0; i < (POW(numOfElements) - d); i++)
		{
			if (matrix[(i + d) / numOfElements][(i + d) % numOfElements] > matrix[i / numOfElements][i%numOfElements])
			{
				SWAP(matrix[(i + d) / numOfElements][(i + d) % numOfElements], matrix[i / numOfElements][i%numOfElements]);
				/* swap positions i+d and i */
				flag = 1;  /* tells swap has occurred */
			}
		}
	}

	printf("\n%sMATRIX:\n", "  ");
	for (uc i = 0; i < numOfElements; printf("\n"), ++i)
		for (uc j = 0; j < numOfElements; ++j)
			printf("%3.0d", matrix[i][j]);

	return 0;
}

/* Code by: Aleksa M. */