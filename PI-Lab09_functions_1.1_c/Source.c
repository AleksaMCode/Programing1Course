#include <stdio.h>
#include <math.h>
#define SIZE 20
typedef unsigned short us;
typedef enum { FALSE, TRUE }BOOL;

BOOL isPrime(int);

int main()
{
	int array[SIZE], n;
	do printf("Enter size of an array: "), scanf("%d", &n); while (n < 1 || n > SIZE);
	
	for (us i = 0; i < (us)n; ++i)
		printf("array[%hu] = ", i + 1), scanf("%d", array + i);

	for (us i = 0; i < (us)n; ++i)
		if (isPrime(*(array + i))) printf("%d ", array[i]);

	return 0;
}

BOOL isPrime(int candidate)
{
	if (candidate > 0)
	{
		unsigned char flag = 1;
		for (int i = 2; i <= (int)sqrt(candidate) && flag; ++i)
			if (!(candidate % i))
				flag = 0;

		return flag ? TRUE : FALSE;
	}
	return FALSE;
}

/* Code by: Aleksa M. */