#include <stdio.h>
#include <math.h>
#define SIZE 20
typedef unsigned char uc;

#define ISPRIME(candidate) \
if (candidate > 0) \
{ \
	uc flag = 1; \
	for (int k = 2; k <= (int)sqrt(candidate) && flag; ++k) \
		if (!(candidate % k)) \
			flag = 0; \
\
	if (flag) printf("%d ", candidate); \
}\

#define PRINT_MATRIX_IF(mat, athm) \
for (uc i = 0; i < athm; ++i) \
for (uc j = 0; j < athm; ++j) \
	if(mat[i][j] > athm) \
	ISPRIME(mat[i][j]); \

int main(void)
{
	int matrix[SIZE][SIZE], numOfElements;
	do printf("Insert matrix dimension: "), scanf("%d", &numOfElements); while (numOfElements < 1 || numOfElements > SIZE);

	for (uc i = 0; i < numOfElements; ++i)
		for (uc j = 0; j < numOfElements; ++j)
			printf("matrix[%hu][%hu]: ", i + 1, j + 1), scanf("%d", &matrix[i][j]);

	printf("\n  MATRIX:\n");
	for (uc i = 0; i < numOfElements; printf("\n"), ++i)
		for (uc j = 0; j < numOfElements; ++j)
			printf("%3.0d", matrix[i][j]);

	double arithmetic_mean_antidiagonal = 0.0;
	for (uc i = 0; i < numOfElements; arithmetic_mean_antidiagonal += matrix[i][numOfElements - 1 - i++]);
	arithmetic_mean_antidiagonal /= numOfElements;

	printf("The requested elements of the matrix are: ");
	PRINT_MATRIX_IF(matrix, arithmetic_mean_antidiagonal);

	return 0;
}

/* Code by: Aleksa M. */