#include <stdio.h>
#include <math.h>
#define SIZE 20
typedef unsigned char uc;
typedef unsigned short us;
#define ISPRIME(candidate) \
if (candidate > 0) \
{ \
	uc flag = 1; \
	for (int k = 2; k <= (int)sqrt(candidate) && flag; ++k) \
		if (!(candidate % k)) \
			flag = 0; \
\
	if (flag) printf("%d ", candidate); \
}\

int main()
{
	int matrix[SIZE][SIZE], numOfElements;
	do printf("Insert matrix dimension: "), scanf("%d", &numOfElements); while (numOfElements < 1 || numOfElements > SIZE);

	for (uc i = 0; i < numOfElements; ++i)
		for (uc j = 0; j < numOfElements; ++j)
			printf("matrix[%hu][%hu]: ", i + 1, j + 1), scanf("%d", &matrix[i][j]);

	printf("\nPRIME NUMBER:\n");
	for (uc i = 0; i < numOfElements; ++i)
		for (uc j = 0; j < numOfElements; ++j)
			ISPRIME(matrix[i][j]);

	return 0;
}

/* Code by: Aleksa M. */