#include <stdio.h>
#include <stdlib.h>
#include <string.h>

void read(char*); /* delimiter = '\n' */
void printFirstSentenceWithStringh(char*); /* !!!!!!delimiter = '.' */

int main()
{
	char string[100];
	read(string);
	if (!strstr(string, ".")) return 1;
	printFirstSentenceWithStringh(string);

	char *string_copy = calloc(strlen(string) + 1, sizeof(char));
	strcpy(string_copy, string);
	char* pch = strtok(string, " ");
	while (pch != NULL)
	{
		char* pstr = strstr(string_copy, pch);
		printf("%d ", pstr - string_copy);
		pch = strtok(NULL, " ");
	}
	free(string_copy);
	return 0;
}

void read(char* str)
{
	fgets(str, 100, stdin);
}

void printFirstSentenceWithStringh(char* str)
{
	printf("%s\n", strtok(str, "."));
}

/* Code by: Aleksa M. */