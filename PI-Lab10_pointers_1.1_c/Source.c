#include <stdio.h>
#include <math.h>

typedef struct { char c; double x, y; }POINT;
typedef struct { POINT A, B, C; }TRIANGLE;

void read(POINT*);
int check(POINT*);
TRIANGLE triangle(POINT*);

int main()
{
	POINT array[3];
	TRIANGLE t;
	for (unsigned short i = 0; i < 3; ++i)
		read(array + i);

	if (check(array)) t = triangle(array), printf("\nTriangle is successfully formed.\n");
	else return printf("\nPoints you entered cannot create a valid triangle.\n"), 1;
	return 0;
}

void read(POINT* point)
{
	static int num_of_points;
	printf("\n%d. POINT:\n", ++num_of_points);
	printf("LETTER: "), scanf(" %c", &point->c);
	printf("X= "), scanf("%lf", &point->x);
	printf("Y= "), scanf("%lf", &point->y);
}

int check(POINT* arr_point)
{
	double a, b, c;
	c = sqrt(pow(arr_point[0].x - arr_point[1].x, 2) + pow(arr_point[0].y - arr_point[1].y, 2));
	b = sqrt(pow(arr_point[0].x - arr_point[2].x, 2) + pow(arr_point[0].y - arr_point[2].y, 2));
	a = sqrt(pow(arr_point[2].x - arr_point[1].x, 2) + pow(arr_point[2].y - arr_point[1].y, 2));

	return (int)((((a + b + c) / 2.0) - a)*(((a + b + c) / 2.0) - b)*(((a + b + c) / 2.0) - c));
}

TRIANGLE triangle(POINT* arr_point)
{
	TRIANGLE t = { arr_point[0], arr_point[1], arr_point[2] };
	return t;
}

/* Code by: Aleksa M. */
