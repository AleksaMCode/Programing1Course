/* GROUP B - 4th task */
#include <stdio.h>
#include <stdlib.h>

char** read(int* pn);
int sorted(char* s);
char** form(char **str, int n, int *brs);

int main()
{
	int n;
	char** sorted = read(&n);
	printf("Sorted:");

	for (int i = 0; i < n; i++)
	{
		printf(" %s", sorted[i]);
		free(sorted[i]);
	}

	free(sorted);
	return 0;
}

char** read(int* pn)
{
	do printf("n = "), scanf("%d", pn); while (*pn < 0);
	char** string = calloc(*pn, sizeof(char*));
	int count = 0;

	for (int i = 0; i < *pn; i++)
	{
		char word[100];
		printf("%d. word: ", i + 1), scanf("%s", word);
		int strlen = 0;
		char* pword = word;

		while (*pword)
		{
			strlen++;
			pword += 1;
		}

		string[i] = calloc(strlen + 1, sizeof(char));

		for (int j = 0; j < strlen; j++)
			string[i][j] = word[j];

		if (sorted(string[i]))
			count++;
	}

	char** stringSorted = form(string, *pn, &count);

	/* free string */
	for (int i = 0; i < *pn; i++)
		free(string[i]);

	free(string);

	*pn = count;
	return stringSorted;
}

int sorted(char* s)
{
	int strlen = 0;

	while (s[strlen] && s[strlen+1])
		if (s[++strlen] <= s[strlen - 1])
			return 0;

	return 1;
}

char** form(char **str, int n, int *brs)
{
	char** stringSorted = calloc(*brs, sizeof(char*));
	int index = 0;

	for (int i = 0; i < n; i++)
		if (sorted(str[i]))
		{
			int strlen = 0;
			while (str[i][strlen] != '\0')
				strlen++;

			stringSorted[index] = calloc(strlen + 1, sizeof(char));

			for (int j = 0; j < strlen; j++)
				stringSorted[index][j] = str[i][j];

			index++;
		}

	return stringSorted;
}

/* mailto:aleksa */
/* Copyright(C) 2017 Aleksa Majkic */
