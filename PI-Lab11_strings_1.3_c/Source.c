#include <stdio.h>
#include <string.h>
#include <ctype.h>

int strlen_(char*);
void change(char*, int);
void change2(char*, int);

int main()
{
	char word[50];
	scanf("%s", word);
	change(word, strlen(word));
/*  change2(word, strlen_(word)); */
	printf("%s\n", word);
	return 0;
}

int strlen_(char* str)
{
	unsigned char i = 0;
	while (*(str + i++));
	return (int)--i;
}

void change(char* str, int length)
{
	if (islower(str[0])) str[0] = toupper(str[0]);

	for (int i = 1; i < length; ++i)
		if (isupper(str[i]))
			str[i] = tolower(str[i]);
}

void change2(char* str, int length)
{
	if (str[0] >= 97 && str[0] <= 122)
		str[0] -= 32;
	for (int i = 1; i < length; ++i)
		if (str[i] >= 65 && str[i] <= 90)
			str[i] += 32;
	/* OR -> then lenght is excess parameter */ /*
	int i = 1;
	while (*(str + i))
		if (str[i] >= 65 && str[i++] <= 90)
			str[i - 1] += 32; */
}

/* Code by: Aleksa M. */