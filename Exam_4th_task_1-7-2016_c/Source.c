#include <stdio.h>
#include <math.h>
#include <stdlib.h>
#define SIZE 100

typedef struct polynomial { int deg; double array[SIZE]; }POLYNOMIAL;

POLYNOMIAL* read();
double calculate(POLYNOMIAL* p, double* x);
POLYNOMIAL product(POLYNOMIAL* p1, POLYNOMIAL* p2);
void print(POLYNOMIAL* p);

int main()
{
	POLYNOMIAL* p1 = NULL, *p2 = NULL;
	p1 = read();
	p2 = read();
	double x;
	printf("\nEnter x: "), scanf("%lf", &x);
	printf("Value of polynomial p1 in point %.2lf: %.2lf\n", x, calculate(p1, &x));
	printf("Value of polynomial p2 in point %.2lf: %.2lf\n", x, calculate(p2, &x));
	POLYNOMIAL mul = product(p1, p2);
	printf("\nProduct: "), print(&mul);

	free(p1);
	free(p2);
	return 0;
}

POLYNOMIAL* read()
{
	static short k;
	POLYNOMIAL* temp = malloc(sizeof(POLYNOMIAL));
	printf("%2d. polynomial", ++k);
	do { printf("\n    degree: "), scanf("%d", &temp->deg); } while (temp->deg < 0 || temp->deg >= SIZE);

	printf("    coefficients: ");
	/* Read the coefficients into an array */
	for (int i = temp->deg; i >= 0; i -= 1)
	{
		double coeff;
		scanf("%lf", &coeff);
		temp->array[i] = coeff;
	}

	return temp;
}

double calculate(POLYNOMIAL* p, double* x)
{
	int deg = p->deg;
	double result = 0.0;
	for (int i = p->deg; i >= 0; i--)
		result += pow(*x, deg--) * p->array[i];

	return result;
}

POLYNOMIAL product(POLYNOMIAL* p1, POLYNOMIAL* p2)
{
	POLYNOMIAL temp = { p1->deg + p2->deg, 0 };
	for (int i = 0; i <= p1->deg; i++)
		for (int j = 0; j <= p2->deg; j++)
			temp.array[i + j] += p1->array[i] * p2->array[j];

	return temp;
}

void print(POLYNOMIAL* p)
{
	printf("%.2lf*x^%d", fabs(p->array[p->deg]), p->deg);
	for (int i = p->deg - 1; i >= 0; i--)
	{
		if (p->array[i] > 0) printf(" + ");
		else if (p->array[i] < 0) printf(" - ");
		i == 1 ? printf("%.2lf*x", fabs(p->array[i])) : i ? printf("%.2lf*x^%d", fabs(p->array[i]), i) : printf("%.2lf", fabs(p->array[i]));
	}
	printf("\n");
}

/* Code by: Aleksa M. */