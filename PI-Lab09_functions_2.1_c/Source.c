#include <stdio.h>
#include <stdbool.h> /* bool */
#include <stdlib.h> /* qsort */
#include <math.h>
#define SIZE 20
typedef unsigned short us;

int n;

void read(int[]);
void print(int[], int);
bool isPerfect(int); /* for example: 6, 28, 496, 8128, etc. */
bool isPrime(int);
int cmp_int(const void*, const void*);
void sort_array(int*, int);

int main()
{
	int array[SIZE], arr2[SIZE], arr3[SIZE], j = 0, k = 0;

	read(array);
	sort_array(array, n);
	printf("\n\n");
	print(array, n);

	bool check = false;
	for (us i = 0; i < (us)n; ++i)
		if (isPrime(*(array + i))) arr2[j++] = *(array + i), check = true;
	check ? sort_array(arr2, j), print(arr2, j) : printf("There are no prime numbers in your array!\n");
	check = false;

	for (us i = 0; i < (us)n; ++i)
		if (isPerfect(*(array + i))) arr3[k++] = array[i], check = true;
	check ? sort_array(arr3, k), print(arr3, k) : printf("There are no perfect numbers in your array!\n");

	return 0;
}

bool isPrime(int candidate)
{
	if (candidate <= 0) return false;
	unsigned char flag = 1;
	for (int i = 2; i <= (int)sqrt(candidate) && flag; ++i)
		if (!(candidate % i))
			flag = 0;

	return flag ? true : false;
}

int cmp_int(const void* el1, const void* el2)
{
	return *(int*)el1 - *(int*)el2;
}

void sort_array(int* array, int size)
{
	qsort(array, size, sizeof(int), cmp_int); /* using cmp_int as an address of a function (function pointers) */
}

void read(int array[])
{
	do printf("Enter size of an array: "), scanf("%d", &n); while (n < 1 || n > SIZE);
	for (us i = 0; i < (us)n; ++i)
		printf("array[%hu] = ", i + 1), scanf("%d", array + i);
}

void print(int array[], int n)
{
	printf("array [ %d", *array);
	for (us i = 1; i < (us)n; ++i)
		printf(", %d", array[i]);
	printf(" ]\n");
}

bool isPerfect(int number)
{
	if (number <= 0) return false;
	int sum = 0;
	for (int i = 1; i <= (number - 1); i++)
		if ((number % i) == 0)
			sum = sum + i;
	return sum == number;
}

/* Code by: Aleksa M. */
