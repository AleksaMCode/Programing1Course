#include <stdio.h>
#include <math.h>
#define POW(x) (x)*(x)

typedef struct { char c; double x, y; }POINT;
typedef struct { POINT A, B, C; }TRIANGLE;
double area_;

void read(POINT*);
int check(POINT*);
TRIANGLE triangle(POINT*);
double euclidean_distance(POINT*, POINT*);
void circumference_and_area(TRIANGLE, double*, double*);
int pointInTriangle(POINT, TRIANGLE); /* Solved using Barycentric coordinate system */

int main()
{
	POINT array[4];
	TRIANGLE t;
	for (unsigned short i = 0; i < 4; ++i)
		read(array + i);

	if (check(array)) t = triangle(array), printf("\nTriangle is successfully formed.\n{ %c(%.2lf, %.2lf), %c(%.2lf, %.2lf), %c(%.2lf, %.2lf) }\n",
		t.A.c, t.A.x, t.A.y, t.B.c, t.B.x, t.B.y, t.C.c, t.C.x, t.C.y);
	else return printf("\nPoints you entered cannot create a valid triangle.\n"), 1;

	printf("Point %c(%.2lf, %.2lf) is %s a triangle.\n", array[3].c, array[3].x, array[3].y, pointInTriangle(array[3], t) ? "in" : "not in");

	double circumference, area;
	circumference_and_area(t, &circumference, &area);
	printf("circumference = %.2lf, area = %.2lf\n", circumference, area);

	return 0;
}

void read(POINT* point)
{
	static int num_of_points;
	printf("\n%d. POINT:\n", ++num_of_points);
	printf("LETTER: "), scanf(" %c", &point->c);
	printf("X= "), scanf("%lf", &point->x);
	printf("Y= "), scanf("%lf", &point->y);
}

int check(POINT* arr_point)
{
	double a, b, c;
	c = euclidean_distance(arr_point, arr_point + 1);
	b = euclidean_distance(arr_point, arr_point + 2);
	a = euclidean_distance(arr_point + 2, arr_point + 1);

	return (int)((((a + b + c) / 2.0) - a)*(((a + b + c) / 2.0) - b)*(((a + b + c) / 2.0) - c));
}

TRIANGLE triangle(POINT* arr_point)
{
	TRIANGLE t = { arr_point[0], arr_point[1], arr_point[2] };
	return t;
}

double euclidean_distance(POINT* p1, POINT* p2)
{
	return sqrt((double)(POW(p1->x - p2->x) + POW(p1->y - p2->y)));
}

void circumference_and_area(TRIANGLE t, double* circumference, double* area)
{
	double a = euclidean_distance(&t.A, &t.B),
		b = euclidean_distance(&t.A, &t.C),
		c = euclidean_distance(&t.B, &t.C);
	/* double s = (a + b + c) / 2.0;
	*area = sqrt(s*(s - a)*(s - b)*(s - c)); */
	*area = area_ > 0 ? area_ : area_*(-1);
	*circumference = a + b + c;
}

int pointInTriangle(POINT p, TRIANGLE tr)
{
	double p0x = tr.A.x, p0y = tr.A.y, p1x = tr.B.x, p1y = tr.B.y, p2x = tr.C.x, p2y = tr.C.y, px = p.x, py = p.y;
	area_ = 0.5 *(-p1y*p2x + p0y*(-p1x + p2x) + p0x*(p1y - p2y) + p1x*p2y);
	double s = 1 / (2 * area_)*(p0y*p2x - p0x*p2y + (p2y - p0y)*px + (p0x - p2x)*py),
		t = 1 / (2 * area_)*(p0x*p1y - p0y*p1x + (p0y - p1y)*px + (p1x - p0x)*py);

	return s > 0 && t > 0 && 1 - s - t > 0; /* for limit cases  use >= instead of > (s>= and t>=0) */
}

/* Code by: Aleksa M. */