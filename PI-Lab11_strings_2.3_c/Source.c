#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include <string.h>
#include <stdbool.h>
/* Assume that there are no incorrect entered words e.g. wo3rd, wo!rd ... */

typedef struct { char* word; int repetition; }CHAR;

char* to_lower(char*);
bool check_for_duplicate(CHAR*, int, char*);
int cmp_repetition(const void*, const void*);
bool check_word_repetition(CHAR*, int);

int main()
{
	char text[5000];
	printf(">> ");
	scanf("%[^\n]s", text);
	CHAR* words = malloc(sizeof(CHAR) * 1000);
	char* pch = strtok(text, " ,.!?123456789-/*+=");
	int count = 0;
	words[count].word = calloc(strlen(pch) + 1, sizeof(char)), strcpy(words[count].word, to_lower(pch)), words[count++].repetition = 1;
	pch = strtok(NULL, " ,.!?123456789-/*+=");
	while (pch != NULL && count < 1000)
	{
		if (strlen(pch) != 1)
		{
			if (!check_for_duplicate(words, count, to_lower(pch)))
			{
				words[count].word = calloc(strlen(pch) + 1, sizeof(char));
				words[count].repetition = 1;
				strcpy(words[count++].word, pch);
			}
		}
		pch = strtok(NULL, " ,.!?123456789-/*+=");
	}

	qsort(words, count, sizeof(CHAR), &cmp_repetition);
	if (check_word_repetition(words, count))
		printf("The most repeated word is [ %s ] -> %d reps\n", words[0].word, words[0].repetition);
	else printf("All the words (total %d) are repeated the same number of times.\n", count);

	for (int i = 0; i < count; i++)
		free(words[i].word);
	free(words);

	return 0;
}

char* to_lower(char* str)
{
	int i = 0;
	while (str[i])
		*(str + i) = tolower(*(str + i)), i++;
	return str;
}

bool check_for_duplicate(CHAR* pstr, int n, char* word)
{
	for (int i = 0; i < n; i++)
		if (!strcmp(pstr[i].word, word))
		{
			pstr[i].repetition++;
			return true;
		}
	return false;
}

int cmp_repetition(const void* a, const void* b)
{
	return (*(CHAR*)a).repetition < (*(CHAR*)b).repetition;
}

bool check_word_repetition(CHAR* struc, int n)
{
	for (int i = 1; i < n; i++)
		if (struc[0].repetition != struc[i].repetition)
			return true;
	return false;
}

/* Code by: Aleksa M. */