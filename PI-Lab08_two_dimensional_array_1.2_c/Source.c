#include <stdio.h>
#define SIZE 20
#define SPACE "  "
#define adrNOE &numOfElements
typedef unsigned char uc;

int main(void)
{
	int matrix[SIZE][SIZE], numOfElements;
	do printf("Insert matrix dimension: "), scanf("%d", adrNOE); while (numOfElements < 1 || numOfElements > SIZE);

	for (uc i = 0; i < numOfElements; ++i)
		for (uc j = 0; j < numOfElements; ++j)
			printf("matrix[%hu][%hu]: ", i + 1, j + 1), scanf("%d", &matrix[i][j]);

	printf("\n%sMATRIX:\n", SPACE);
	for (uc i = 0; i < numOfElements; printf("\n"), ++i)
		for (uc j = 0; j < numOfElements; ++j)
			printf("%3.0d", matrix[i][j]);

	short sum_main_diagonal = 0, sum_antidiagonal = 0; /* antidiagonal = counterdiagonal, secondary diagonal, trailing diagonal or minor diagonal */
	for (uc i = 0; i < numOfElements; sum_main_diagonal += matrix[i][i], sum_antidiagonal += matrix[i][*adrNOE - 1 - i++]);
	return printf("Sum of main diagonal: %hd\nSum of antidiagonal: %hd\n", sum_main_diagonal, sum_antidiagonal), 0;
}

/* Code by: Aleksa M. */