/* GROUP A */
#include <stdio.h>
#include <stdlib.h>

char** read(int* pn);
int palindrome(char* s);
char** form(char **str, int n, int *brp);

int main()
{
	int n;
	char** palindromes = read(&n);
	printf("Palindromes:");

	for (int i = 0; i < n; i++)
	{
		printf(" %s", palindromes[i]);
		free(palindromes[i]);
	}

	free(palindromes);
	return 0;
}

char** read(int* pn)
{
	do printf("n = "), scanf("%d", pn); while (*pn < 0);
	char** string = calloc(*pn, sizeof(char*));
	int count = 0;

	for (int i = 0; i < *pn; i++)
	{
		char word[100];
		printf("%d. word: ", i + 1), scanf("%s", word);
		int strlen = 0;
		char* pword = word;

		while (*pword)
		{
			strlen++;
			pword += 1;
		}

		string[i] = calloc(strlen + 1, sizeof(char));

		for (int j = 0; j < strlen; j++)
			string[i][j] = word[j];

		if (palindrome(string[i]))
			count++;
	}

	char** stringPalindrome = form(string, *pn, &count);

	/* free string */
	for (int i = 0; i < *pn; i++)
		free(string[i]);

	free(string);

	*pn = count;
	return stringPalindrome;
}

int palindrome(char* s)
{
	int strlen = 0;

	while (s[strlen] != '\0')
		strlen++;

	for (int i = 0; i < strlen / 2; i++)
		if (s[i] != s[strlen - i - 1])
			return 0;

	return 1;
}

char** form(char **str, int n, int *brp)
{
	char** stringPalindrome = calloc(*brp, sizeof(char*));
	int index = 0;

	for (int i = 0; i < n; i++)
	{
		/* if a word is palindrome, copy it to stringPalindrome */
		if (palindrome(str[i]))
		{
			int strlen = 0;
			while (str[i][strlen] != '\0')
				strlen++;

			stringPalindrome[index] = calloc(strlen + 1, sizeof(char));

			for (int k = 0; k < strlen; k++)
				stringPalindrome[index][k] = str[i][k];

			index++;
		}
	}
	return stringPalindrome;
}

/* Code by: Aleksa M. */