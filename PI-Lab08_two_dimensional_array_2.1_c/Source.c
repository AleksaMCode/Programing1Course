#include <stdio.h>
#define SIZE 20
typedef unsigned char uc;
#define PRINT_MATRIX(mat, n) \
for (uc i = 0; i < n; printf("\n"), ++i) \
for (uc j = 0; j < n; ++j) \
	printf("%3.0d", mat[i][j]); \

int main()
{
	int matrix[SIZE][SIZE], numOfElements;
	do printf("Insert matrix dimension: "), scanf("%d", &numOfElements); while (numOfElements < 1 || numOfElements > SIZE);

	for (uc i = 0; i < numOfElements; ++i)
		for (uc j = 0; j < numOfElements; ++j)
			printf("matrix[%hu][%hu]: ", i + 1, j + 1), scanf("%d", &matrix[i][j]);

	printf("\n  MATRIX:\n");
	PRINT_MATRIX(matrix, numOfElements);

	short sum_above = 0, sum_below = 0;
	/*
	for (uc i = 0; i < numOfElements; i++)
		for (uc j = i + 1; j < numOfElements; j++)
			sum_above += matrix[i][j];

	for (uc i = 1; i < numOfElements; i++)
		for (uc j = 0; j < i; j++)
			sum_below += matrix[i][j];
	*/
	for (uc i = 0; i < numOfElements; ++i)
		for (uc j = 0; j < numOfElements; ++j)
			if (j > i) sum_above += (short)matrix[i][j];
			else if (j < i) sum_below += (short)matrix[i][j];

	printf("Sum above main diagonal: %hd\nSum below main diagonal: %hd\n", sum_above, sum_below);
	sum_above = sum_below = 0;

	/*
	for (uc i = 0; i < numOfElements; i++)
		for (uc j = 0; j < numOfElements - 1 - i; j++)
			sum_above += matrix[i][j];

	for (uc i = 1; i < numOfElements; i++)
		for (uc j = numOfElements - i; j < numOfElements; j++)
			sum_below += matrix[i][j];
	*/
	for (uc i = 0; i < numOfElements; ++i)
		for (uc j = 0; j < numOfElements; ++j)
			if (j < numOfElements - i - 1) sum_above += (short)matrix[i][j];
			else if (j > numOfElements - i - 1) sum_below += (short)matrix[i][j];

	printf("Sum above antidiagonal: %hd\nSum below antidiagonal: %hd\n", sum_above, sum_below);

	return 0;
}

/* Code by: Aleksa M. */