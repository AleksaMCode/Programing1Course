/* Exam 7.7.2015. - 3rd task */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#define SIZE_MAX_OF_BIG_INT 100

typedef struct
{
	char bigint_char[SIZE_MAX_OF_BIG_INT * 2 + 1];
	size_t number_of_digits;
}BigInt;

void read(BigInt* number);
void square(BigInt* number, BigInt* result);
void print(BigInt* number);
int n; /* count of numbers */

int main()
{
	do printf("n= "), scanf("%d", &n); while (n < 0 || n > SIZE_MAX_OF_BIG_INT);
	BigInt* array = malloc(n * sizeof(BigInt));
	memset(array, 0, n);
	read(array);
	print(array);

	free(array);
	return 0;
}

void read(BigInt* number)
{
	int i = 0;

	char number_char[SIZE_MAX_OF_BIG_INT + 1];
	do
	{
		do printf("%d. number: ", i + 1), scanf("%s", number_char); while (!strlen(number_char) || strlen(number_char) > SIZE_MAX_OF_BIG_INT);
		register short check = 0;
		for (int i = 0; i < strlen(number_char); i++)
			if (!isdigit(number_char[i]))
			{
				check = 1;
				break;
			}

		if (check) continue;
		else
		{
			number[i].number_of_digits = strlen(number_char);
			strcpy(number[i++].bigint_char, number_char);
		}

	} while (i < n);
}

void square(BigInt* number, BigInt* result)
{
	int number_int[SIZE_MAX_OF_BIG_INT];
	int ans[SIZE_MAX_OF_BIG_INT * 2] = { 0 }; /* ans = answer */
	int length = (int)number->number_of_digits;

	for (int i = length - 1, j = 0; i >= 0; i--, j++)
		number_int[j] = number->bigint_char[i] - '0';

	for (int i = 0; i < length; i++)
		for (int j = 0; j < length; j++)
			ans[i + j] += number_int[i] * number_int[j];

	for (int i = 0; i < length * 2; i++)
	{
		int temp = ans[i] / 10;
		ans[i] %= 10;
		ans[i + 1] += temp;
	}

	/* reverse an array ans and write in result */
	int i;
	for (i = length * 2; i >= 0; i--)
		if (ans[i] > 0)
			break;

	for (int j = 0; i >= 0; i--)
		result->bigint_char[j++] = ans[i] + '0';
}

void print(BigInt* number)
{
	printf("--------------------------------------------------\nResult:\n");
	for (int i = 0; i < n; i++)
	{
		BigInt result = { 0,0 };
		square(&number[i], &result);
		printf("sq(%s)=%s\n", (number + i)->bigint_char, result.bigint_char);
	}
}

/* mailto:aleksa */
/* Copyright(C) 2017 Aleksa Majkic */