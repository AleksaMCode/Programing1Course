#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include <string.h>
#include <stdbool.h>
/* Assume that there are no incorrect entered words e.g. wo3rd, wo!rd ... */

char* to_lower(char*);
bool check_for_duplicate(char**, int, char*);

int main()
{
	int size = 100;  /* Size of text divided with 5 -> The average length of a word in most documents is just over 5. ( average english word length ) */
	char text[500];
	printf(">> ");
	scanf("%[^\n]s", text);
	char** pwords = malloc(sizeof(char*)*size);
	char* pch = strtok(text, " ,.!?123456789-/*+=");
	int count = 0, count2 = 0;
	pwords[count] = calloc(strlen(pch) + 1, sizeof(char)), strcpy(pwords[count2++, count++], to_lower(pch));
	pch = strtok(NULL, " ,.!?123456789-/*+=");
	while (pch != NULL)
	{
		if (strlen(pch) != 1)
		{
			if (!check_for_duplicate(pwords, count, to_lower(pch)))
			{
				if (count == size) pwords = realloc(pwords, size *= 2);
				pwords[count] = calloc(strlen(pch) + 1, sizeof(char));
				strcpy(pwords[count++], pch);
			}
			count2++;
		}
		pch = strtok(NULL, " ,.!?123456789-/*+=");
	}
	printf("\nTotal of %d words in text.\nTotal of %d different words in text.\n", count2, count);
	for (int i = 0; i < count; i++)
		free(pwords[i]);
	free(pwords);

	return 0;
}

char* to_lower(char* str)
{
	int i = 0;
	while (str[i])
		*(str + i) = tolower(*(str + i)), i++;
	return str;
}

bool check_for_duplicate(char** pstr, int n, char* word)
{
	for (int i = 0; i < n; i++)
		if (!strcmp(pstr[i], word))
			return true;

	return false;
}

/* Code by: Aleksa M. */