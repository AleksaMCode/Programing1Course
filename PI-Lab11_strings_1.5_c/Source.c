#include <stdio.h>
#include <string.h>
#include <stdlib.h>

int main()
{
	int n;
	char word[100];
	printf("n= "), scanf("%d", &n);
	char** pword = malloc(sizeof(char*)*n);
	for (int i = 0; i < n; ++i)
	{
		printf("%d. word: ", i + 1), scanf("%s", word);
		pword[i] = calloc(strlen(word) + 1, sizeof(char));
		strcpy(pword[i], word);
	}
	printf("\n\n");
	for (int i = 0; i < n; ++i)
		printf("%s ", pword[i]), free(pword[i]);
	free(pword);

	return 0;
}

/* Code by: Aleksa M. */
